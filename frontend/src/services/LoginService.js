export default class LoginService {
    static async login() {
        console.log('Client: login');

        let login = document.querySelector('#username').value;
        let password = document.querySelector('#password').value;

        let body = JSON.stringify({username: login, password: password});

        return await fetch('/auth/login', {
                method: 'POST',
                body: body,
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                }
            })
            .then(res => {
                return res.json()
            })
            .then(json => {
                if (json != null) {
                    window.location.href = '/';
                    return "";
                }

                return "You could not be logged in. Please verify your username and password."
            })
            .catch(err => {
                console.log(err);
                return "Something went wrong during login."
            });
    }

    static logout() {
        console.log('Client: logout');

        fetch('/auth/logout', {method: 'POST'})
            .then(res => {
                console.log('Client: to login');
                window.location.href = '/login';
            });
    }
}