import React, { Component } from 'react';
import { TextField } from '@mui/material';

class RequiredTextField extends Component {
    state = {
        filled: true,
        text: ""
    }

    constructor(props) {
        super(props);
        this.checkFilled = this.checkFilled.bind(this);
    }

    checkFilled(event) {
        let newState = this.state;

        newState.filled = event.target.value !== '';

        if (!newState.filled) {
            newState.text = "This field is required";
        } else {
            newState.text = "";
        }

        this.setState(newState);
        this.props.onInput();
    }

    render() {
        return(
            <TextField
                error={!this.state.filled}
                required
                id={this.props.id}
                label={this.props.label}
                type={this.props.type}
                autoComplete={this.props.autoComplete}
                onInput={this.checkFilled}
                helperText={this.state.text}
            />
        );
    }
}

export default RequiredTextField;