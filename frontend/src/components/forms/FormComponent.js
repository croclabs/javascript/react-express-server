import { Box } from '@mui/material';
import React, { Component } from 'react';

class FormComponent extends Component {
    render() {
        return (
            <Box sx={{ 
                marginTop: '7px',
                marginBottom: '7px'
            }}>
                {this.props.children}
            </Box>
        );
    }
}

export default FormComponent;