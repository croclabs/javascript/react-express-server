import React, { Component } from 'react';
import Center from '../design/Center';
import Full from '../design/Full';
import { Alert, Button } from '@mui/material';
import FormCard from '../forms/FormCard';
import FormComponent from '../forms/FormComponent';
import LoginService from '../../services/LoginService';
import RequiredTextField from '../forms/RequiredTextField';

class Login extends Component {
    state = {
        disabled: true,
        loginError: ''
    }

    constructor(props) {
        super(props);

        this.login = this.login.bind(this);
        this.checkInputs = this.checkInputs.bind(this);
    }

    checkInputs() {
        let username = document.getElementById('username').value;
        let password = document.getElementById('password').value;

        let newState = this.state;

        newState.disabled = username === '' || password === '';
        this.setState(newState);
    }

    login() {
        LoginService.login()
            .then(result => {
                let newState = this.state;
                
                newState.loginError = result;
                this.setState(newState);
            });
    }

    render() {
        return (
            <Full>
                <Center>
                    <FormCard marginAuto>
                        <FormComponent>
                            <h1 style={{marginTop: '0', textAlign: 'center'}}>Login</h1>
                        </FormComponent>

                        {this.state.loginError.length > 0 && <Alert sx={{width: '180px', marginBottom: '20px'}} severity="error">{this.state.loginError}</Alert>}

                        <FormComponent>
                            <RequiredTextField
                                id="username"
                                label="Username"
                                onInput={this.checkInputs}
                            />
                        </FormComponent>
                        <FormComponent>
                            <RequiredTextField
                                id="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                onInput={this.checkInputs}
                            />
                        </FormComponent>
                        <FormComponent>
                            <Button
                                sx={{width: '100%', marginTop: '20px'}}
                                variant="contained"
                                onClick={this.login}
                                disabled={this.state.disabled}
                            >
                                Login
                            </Button>
                        </FormComponent>
                    </FormCard>
                </Center>
            </Full>
        );
    }
}

export default Login;