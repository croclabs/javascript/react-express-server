let express = require('express');
let router = express.Router();

let HTTP = require('http-module');
const UserService = require('../services/UserService');

/* GET users listing. */
router.get('/user', function(req, res, next) {
    if (req.session.user != null) {
        res.json(req.session.user);
    } else {
        res.json({username: '', loggedIn: false});
    }
});

router.post('/login', function(req, res, next) {
    console.log(req.body);

    let body = req.body;
    let user;
    
    user = UserService.login(body.username, body.password);

    console.log(user);
    req.session.user = user;

    if (user == null) {
        res.json({username: '', loggedIn: false});
    } else {
        res.json(user);
    }
});

router.post('/logout', function(req, res, next) {
    console.log("Logging out...");
    req.session.user = null;
    res.json({message: 'logged out'});
});

module.exports = router;