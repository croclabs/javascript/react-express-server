let express = require('express');
let router = express.Router();

let HTTP = require('http-module');

/* GET users listing. */
router.get('/', function(req, res, next) {
  const options = {
      host: 'redmine.org',
      path: '/issues.json?limit=10',
      method: 'GET'
  };

  HTTP(options)
    .then(issues => {
      res.json(issues);
    });
});

module.exports = router;
